# This is a cross-platform list tracking distribution packages needed by tests;
# see http://docs.openstack.org/infra/bindep/ for additional information.

build-essential [platform:dpkg]
python-dev [platform:dpkg]
# Is required for scipy
python-numpy [platform:dpkg]
gfortran [platform:dpkg]
libatlas-base-dev [platform:dpkg]
liblapack-dev [platform:dpkg]

# FIXME(evgeniyl): Is not required for bareon allocator
# but required for openstack infra not to fail the
# gate, because it tries it initialize the database
mysql-client [platform:dpkg]
mysql-server [platform:dpkg]
postgresql [platform:dpkg]
postgresql-client [platform:dpkg]
